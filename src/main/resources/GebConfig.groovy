import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.FirefoxProfile


waiting {
	timeout = 40
}

environments {
	firefox {
		driver = {
			FirefoxProfile profile = new FirefoxProfile()
			FirefoxOptions firefoxOptions = new FirefoxOptions(profile: profile)
			firefoxOptions.headless = true
			def driverInstance = new FirefoxDriver(firefoxOptions)
			driverInstance.manage().window().maximize()
			driverInstance
		}
	}
}

